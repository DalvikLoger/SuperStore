import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
from sklearn.cluster import KMeans
sns.set_theme() # pour modifier le thème

df = pd.read_csv('Sample - Superstore.csv',header=0, encoding='ISO-8859-1', parse_dates=[0], index_col=0, squeeze=True)

import statsmodels.api as sm
from pmdarima import auto_arima

# Chargement des données
df_copy = df[['Order Date', 'Sales']].copy()
df_copy['Order Date'] = pd.to_datetime(df_copy['Order Date'])

# Resample des données à une fréquence mensuelle et somme des ventes pour chaque mois
monthly_data = df_copy.resample('M', on='Order Date').sum()

# Utilisation de pmdarima pour sélectionner l'ordre optimal
auto_model = auto_arima(monthly_data['Sales'], seasonal=True, m=12)
order = auto_model.get_params()['order']
seasonal_order = auto_model.get_params()['seasonal_order']

# Initialisation et ajustement du modèle SARIMA
model = sm.tsa.SARIMAX(monthly_data['Sales'], order=order, seasonal_order=seasonal_order)
sarima = model.fit()

# Création d'un dataframe futur pour les 12 prochains mois
future = pd.date_range(start=monthly_data.index[-1], periods=13, freq='M')[1:]
future_df = pd.DataFrame({'Order Date': future})

# Prévisions pour le futur
forecast = sarima.get_forecast(steps=12)
forecast_index = pd.DataFrame(index=future, data={'Forecast': forecast.predicted_mean})

# Visualisation des résultats
plt.plot(monthly_data['Sales'], label='Historical Sales')
plt.plot(forecast_index['Forecast'], label='Forecast')
plt.legend()
plt.show()